package main

import (
	"github.com/streadway/amqp"
	"log"
	"time"
	"fmt"
)
func failOnError(err error, msg string) {
	if err != nil {
			log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
			"time-logs",   // name
			"fanout", // type
			true,     // durable
			true,    // auto-deleted
			false,    // internal
			false,    // no-wait
			nil,      // arguments
	)
	failOnError(err, "Failed to declare an exchange")
	for true {
		t := time.Now()
		body := fmt.Sprintf("%02d:%02d", t.Hour(), t.Minute())
		err = ch.Publish(
				"time-logs", // exchange
				"",     // routing key
				false,  // mandatory
				false,  // immediate
				amqp.Publishing{
						ContentType: "text/plain",
						Body:        []byte(body),
				})
		failOnError(err, "Failed to publish a message")
		log.Printf("Current time: %s", body)
		time.Sleep(60 * time.Second)
	}

}